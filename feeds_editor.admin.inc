<?php

/**
 * Form bulder. Basic settings
 */
function feeds_editor_settings_form($form, &$form_state) {
  
  // Set lifespan of uploads.
  $form['feeds_editor_files_lifespan'] = array(
    '#type' => 'select',
    '#title' => t('Delete uploaded files older than'),
    '#options' => array(
      0 => t('Never delete'),
      7200 => t('2 hours'),
      86400 => t('1 day'),
      259200 => t('3 days'),
      864000 => t('10 days'),
      2592000 => t('1 month'),
      15552000 => t('6 month'),
      31104000 => t('1 year'),
    ),
    '#default_value' => variable_get('feeds_editor_files_lifespan', 0),
  );
  
  // Set default working database.
  $form['feeds_editor_working_db'] = array(
    '#type' => 'select',
    '#title' => t('Working database'),
    '#options' => feeds_editor_database_options(),
    '#default_value' => variable_get('feeds_editor_working_db', 'default'),
    '#description' => t('Select default working database used for feeds tables.
      If you decided to use a separate database (which is a good idea),
      you must specify its parameters in the <a href="@url">settings.php</a>', array(
        '@url' => 'http://api.drupal.org/api/drupal/sites!default!default.settings.php/7')),
  );
  
  // How many items process for one batch iteration
  $form['feeds_editor_export_batch_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch limit'),
    '#size' => 3,
    '#default_value' => variable_get('feeds_editor_export_batch_limit', 50),
    '#description' => t('How many rows process for one batch iteration when exporting the grid into downloadable file'),
  );

  $form['feeds_editor_urls_delimiter'] = array(
    '#type' => 'textfield',
    '#title' => t('File URL delimiter'),
    '#size' => 3,
    '#required' => TRUE,
    '#default_value' => variable_get('feeds_editor_urls_delimiter', ','),
    '#description' => t('Which character to use to separate multiple file URLs on file upload form.'),
  );
  return system_settings_form($form);
}

/**
 * Form bulder. Create/edit worksheet
 */
function feeds_editor_admin_form($form, &$form_state, $table = NULL) {

  $form['#tree'] = TRUE;
  $form['id'] = array(
    '#type' => 'value',
    '#value' => !empty($table['id']) ? (int) $table['id'] : NULL,
  );
  
  $arguments = arg();
  
  // Show full edit form, cut unneeded elements in modal window
  if (end($arguments) != 'customize') {
    $form['feeds_id'] = array(
      '#type' => 'select',
      '#title' => t('Feeds ID'),
      '#options' => feeds_editor_feeds_options(),
      '#required' => TRUE,
      '#empty_value' => 0,
      '#default_value' => !empty($table['feeds_id']) ? $table['feeds_id'] : NULL,
      '#weight' => -10,
      '#description' => t('Choose an <a href="@url">existing</a> feeds id', array('@url' => url('admin/structure/feeds'))),
      '#ajax' => array(
        'callback' => 'feeds_editor_admin_table_columns',
        'wrapper' => 'table-settings',
      ),
    );
    
    // Disable feeds id element if the feeds id already set and we'are on edit mode.
    if (!empty($table['feeds_id'])) {
      $form['feeds_id']['#disabled'] = TRUE;
    }
    
    // Choose working database
    $form['db'] = array(
      '#type' => 'select',
      '#title' => t('Working database'),
      '#options' => feeds_editor_database_options(),
      '#default_value' => !empty($table['db']) ? $table['db'] : variable_get('feeds_editor_working_db', 'default'),
      '#description' => t('Select working database for this table.
        If you decided to use a separate database (which is a good idea),
          you must specify its parameters in the <a href="@url">settings.php</a>', array(
            '@url' => 'http://api.drupal.org/api/drupal/sites!default!default.settings.php/7')),
    );
    
    // Human name for the worksheet
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => isset($table['name']) ? check_plain($table['name']) : '',
      '#weight' => 0,
      '#description' => t('Upon submit for this table will be created a database table with the name
        <em>@prefixDATABASE MACHINE NAME</em>', array('@prefix' => FEEDS_EDITOR_DB_PREFIX)),
    );
    
    // Column names in MySql database can not be longer than 64 characters.
    $limit = 64 - strlen(FEEDS_EDITOR_DB_PREFIX);
    $form['db_table'] = array(
      '#type' => 'machine_name',
      '#title' => t('Database machine name'),
      '#maxlength' => $limit,
      '#default_value' => isset($table['db_table']) ? check_plain(str_replace(FEEDS_EDITOR_DB_PREFIX, '', $table['db_table'])) : '',
      '#machine_name' => array(
        'label' => t('Database machine name'),
        'exists' => 'feeds_editor_db_table_exists',
      ),
      '#description' => t('Max characters: @num', array('@num' => $limit)),
    );
    
    // Optional description for administrative purpose
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#maxlength' => 255,
      '#default_value' => isset($table['description']) ? check_plain($table['description']) : '',
      '#weight' => 10,
      '#description' => t('A short description of the table for administrative purpose'),
    );
  }
  
  $form['data'] = array(
    '#type' => 'container',
    '#weight' => 15,
    '#attributes' => array('id' => 'table-settings'),
  );
  
  $feeds_id = NULL;
  if (!empty($form_state['values']['feeds_id'])) {
    $feeds_id = $form_state['values']['feeds_id'];
  }
  elseif (!empty($table['feeds_id'])) {
    $feeds_id = $table['feeds_id'];
  }
  
  if ($feeds_id) {
    // Column settings
    $form['data']['columns'] = array(
      '#type' => 'fieldset',
      '#title' => 'Columns',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => 0,
      '#theme' => 'feeds_editor_admin_table_columns',
    );
    
    // Load importer
    $importer = feeds_importer_load($feeds_id);
    // Get mapping info
    $mappings = $importer->config['processor']['config']['mappings'];
    
    if ($mappings) {
      foreach ($mappings as $key => $map) {
        $source_key = $map['source'] . '|' . $key;
        // These options enabled by default
        $default_value = array('editable', 'search', 'resizable', 'sortable');
        $column_settings = $table['data']['columns']['settings'][$source_key] + $table['data']['columns']['rules'][$source_key];
        
        if (!empty($column_settings)) {
          $default_value = array_keys(array_filter($column_settings));
        }
        
        $form['data']['columns']['settings'][$source_key] = array(
          '#title' => t('Main setting'),
          '#title_display' => 'invisible',
          '#type' => 'select',
          '#multiple' => TRUE,
          '#options' => array(
            // Column options
            'editable' => t('Editable'),
            'hidedlg' => t('Hide in modal'),
            'hidden' => t('Hidden'),
            'resizable' => t('Resizable'),
            'search' => t('Searchable'),
            'sortable' => t('Sortable'),
            'title' => t('Enable title'),
            // Edit rules
            'edithidden' => t('Edit hidden in modal'),
            'required' => t('Required'),
            'number' => t('Check numeric'),
            'integer' => t('Check integer'),
          ),
          '#default_value' => $default_value,
        );
      }
    }
    
    // Store extracted mapping to futher use
    $form['mappings'] = array(
      '#type' => 'value',
      '#value' => $mappings,
    );
  }
  
  // General JqGrid settings
  $form['data']['js'] = array(
    '#type' => 'fieldset',
    '#title' => 'Appearance',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );
  
  // Hide fieldset in modal window
  if (end($arguments) == 'customize') {
    $form['data']['js'] = array('#type' => 'container');
  }
  
  // General settings for grid
  $form['data']['js']['table'] = array(
    '#type' => 'fieldset',
    '#title' => t('Grid'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Settings for toolbar
  $form['data']['js']['navGrid'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tools'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Settings for edit form
  $form['data']['js']['edit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  // Choose Jquery Ui theme
  $form['data']['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme for table'),
    '#options' => feeds_editor_theme_options(),
    '#empty_option' => t('Default'),
    '#empty_value' => 0,
    '#default_value' => isset($table['data']['theme']) ? $table['data']['theme'] : 0,
    '#weight' => -99,
  );
  
  $form['data']['js']['table']['rownumbers'] = array(
    '#type' => 'checkbox',
    '#title' => t('Count rows'),
    '#default_value' => isset($table['data']['js']['table']['rownumbers']) ? $table['data']['js']['table']['rownumbers'] : 0,
    '#description' => t('The purpose of this column is to count the number of available rows.
      If this option is selected, a new column at left of the grid is added.'),
  );

  $form['data']['js']['table']['viewrecords'] = array(
    '#type' => 'checkbox',
    '#title' => t('View records'),
    '#default_value' => isset($table['data']['js']['table']['viewrecords']) ? $table['data']['js']['table']['viewrecords'] : 1,
    '#description' => t('If true, grid displays the beginning and ending record number in the grid,
      out of the total number of records in the query.
        This information is shown in the pager bar (bottom right by default)in this format: "View X to Y out of Z"'),
  );
  
  $form['data']['js']['table']['sortable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sortable columns'),
    '#default_value' => isset($table['data']['js']['table']['sortable']) ? $table['data']['js']['table']['sortable'] : 0,
    '#description' => t('When selected, this option allows reordering columns by dragging and dropping them with the mouse.'),
  );
  
  $form['data']['js']['table']['toppager'] = array(
    '#type' => 'checkbox',
    '#title' => t('Top pager'),
    '#default_value' => isset($table['data']['js']['table']['toppager']) ? $table['data']['js']['table']['toppager'] : 0,
    '#description' => t('When enabled this option places a pager element at top of the grid, below the caption (if available).
      If another pager is defined, both can coexist and are kept in sync'),
  );
  
  $form['data']['js']['table']['forceFit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fit columns'),
    '#default_value' => isset($table['data']['js']['table']['forceFit']) ? $table['data']['js']['table']['forceFit'] : 0,
    '#description' => t('If set to true, and a column\'s width is changed,
      the adjacent column (to the right) will resize so that the overall grid width is maintained
        (e.g., reducing the width of column 2 by 30px will increase the size of column 3 by 30px).
          In this case there is no horizontal scrollbar'),
  );
  
  $form['data']['js']['table']['multiselect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Multiselect'),
    '#default_value' => isset($table['data']['js']['table']['multiselect']) ? $table['data']['js']['table']['multiselect'] : 0,
    '#description' => t('If selected a multi selection of rows is enabled. A new column at left side containing checkboxes is added'),
  );
  
  $form['data']['js']['table']['rowList'] = array(
    '#type' => 'textfield',
    '#title' => t('Row List'),
    '#default_value' => isset($table['data']['js']['table']['rowList']) ? implode(',', $table['data']['js']['table']['rowList']) : '50,100,150',
    '#size' => 10,
    '#description' => t('Used to construct a select box element in the pager in which we can change the number of the visible rows.
      Separate values by comma, no spaces!'),
  );
  
  $form['data']['js']['table']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($table['data']['js']['table']['height']) ? $table['data']['js']['table']['height'] : 500,
    '#size' => 3,
    '#description' => t('The height of the grid.
      Can be set as number (in this case we mean pixels) or as percentage (only 100% is acceped) or value of auto is acceptable'),
  );

  $form['data']['js']['navGrid']['add'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add button'),
    '#default_value' => isset($table['data']['js']['navGrid']['add']) ? $table['data']['js']['navGrid']['add'] : 1,
    '#description' => t('Display "Add row" button'),
  );
  
  $form['data']['js']['navGrid']['edit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Edit button'),
    '#default_value' => isset($table['data']['js']['navGrid']['edit']) ? $table['data']['js']['navGrid']['edit'] : 1,
    '#description' => t('Display "Edit row" button'),
  );
  
  $form['data']['js']['navGrid']['search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Search button'),
    '#default_value' => isset($table['data']['js']['navGrid']['search']) ? $table['data']['js']['navGrid']['search'] : 1,
    '#description' => t('Display "Search" button'),
  );
  
  $form['data']['js']['navGrid']['refresh'] = array(
    '#type' => 'checkbox',
    '#title' => t('Refresh button'),
    '#default_value' => isset($table['data']['js']['navGrid']['refresh']) ? $table['data']['js']['navGrid']['refresh'] : 1,
    '#description' => t('Display "Refresh" button'),
  );
  
  $form['data']['js']['navGrid']['del'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete button'),
    '#default_value' => isset($table['data']['js']['navGrid']['del']) ? $table['data']['js']['navGrid']['del'] : 1,
    '#description' => t('Display "Delete" button'),
  );
  
  $form['data']['js']['edit']['reloadAfterSubmit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reload after submit'),
    '#default_value' => isset($table['data']['js']['edit']['reloadAfterSubmit']) ? $table['data']['js']['edit']['reloadAfterSubmit'] : 1,
    '#description' => t('Reload grid data after posting.'),
  );
  
  $form['data']['js']['edit']['closeOnEscape'] = array(
    '#type' => 'checkbox',
    '#title' => t('Close on Escape'),
    '#default_value' => isset($table['data']['js']['edit']['closeOnEscape']) ? $table['data']['js']['edit']['closeOnEscape'] : 1,
    '#description' => t('When selected the modal window can be closed with ESC key from the user.'),
  );
  
  $form['data']['js']['edit']['checkOnUpdate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Confirm update'),
    '#default_value' => isset($table['data']['js']['edit']['checkOnUpdate']) ? $table['data']['js']['edit']['checkOnUpdate'] : 0,
    '#description' => t('This option is applicable in add and edit mode.
      When this option is set to true the behaviour as follow:
        when something is changed in the form and the user click on Cancel button, navigator buttons,
          close button (on upper right corner of the form),
            in overlay (if available) or press Esc key (if set) a message box appear asking the user to save the changes,
              not to save the changes or go back in the grid cancel all changes (this will close the modal form)'),
  );
  
  $form['data']['js']['edit']['closeAfterEdit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Close after edited'),
    '#default_value' => isset($table['data']['js']['edit']['closeAfterEdit']) ? $table['data']['js']['edit']['closeAfterEdit'] : 1,
    '#description' => t('When in edit mode, close the dialog after editing'),
  );
  
  $form['data']['js']['edit']['closeAfterAdd'] = array(
    '#type' => 'checkbox',
    '#title' => t('Close after added'),
    '#default_value' => isset($table['data']['js']['edit']['closeAfterAdd']) ? $table['data']['js']['edit']['closeAfterAdd'] : 0,
    '#description' => t('When add mode, close the dialog after add record'),
  );
  
  $form['data']['js']['edit']['viewPagerButtons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show pager'),
    '#default_value' => isset($table['data']['js']['edit']['viewPagerButtons']) ? $table['data']['js']['edit']['viewPagerButtons'] : 1,
    '#description' => t('This option enable or disable the appearing of the previous and next buttons (pager buttons) in the form'),
  );
  
  $form['data']['js']['edit']['clearAfterAdd'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear after add'),
    '#default_value' => isset($table['data']['js']['edit']['clearAfterAdd']) ? $table['data']['js']['edit']['clearAfterAdd'] : 1,
    '#description' => t('Clear the data after adding data'),
  );
  
  $default_value = '13';
  $savekey_on = 1;
  if (!empty($table['data']['js']['edit']['savekey'])) {
    $savekey = $table['data']['js']['edit']['savekey'];
    $savekey_on = $savekey[0];
    unset($savekey[0]);
    $default_value = implode(',', $savekey);
  }
  
  $form['data']['js']['edit']['savekey_on'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save on press hotkey'),
    '#default_value' => $savekey_on,
    '#description' => t('Determines the possibility to save the form with pressing a certain key'),
  );

  $form['data']['js']['edit']['savekey'] = array(
    '#type' => 'textfield',
    '#title' => t('"Save" hotkey code'),
    '#default_value' => $default_value,
    '#size' => 6,
    '#description' => t('Please enter a <a href="@url">numeric code</a>', array(
      '@url' => 'http://www.foreui.com/articles/Key_Code_Table.htm')),
    '#states' => array(
      'visible' => array(
        ':input[name="data[js][edit][savekey_on]"]' => array('checked' => TRUE),
      ),
    ),
  );

  $default_value = '38,40';
  $navkeys_on = 1;
  if (!empty($table['data']['js']['edit']['navkeys'])) {
    $navkeys = $table['data']['js']['edit']['navkeys'];
    $navkeys_on = $navkeys[0];
    unset($navkeys[0]);
    $default_value = implode(',', $navkeys);
  }

  $form['data']['js']['edit']['navkeys_on'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigate rows hotkey'),
    '#default_value' => $navkeys_on,
    '#description' => t('This option works only in edit mode and add keyboard navigation,
      which allow us to navigate through the records while in form editing pressing certain keys'),
  );

  $form['data']['js']['edit']['navkeys'] = array(
    '#type' => 'textfield',
    '#title' => t('Navigate rows hotkey code'),
    '#default_value' => $default_value,
    '#size' => 6,
    '#description' => t('Please enter <a href="@url">numeric codes</a> separated by comma', array(
      '@url' => 'http://www.foreui.com/articles/Key_Code_Table.htm')),
    '#states' => array(
      'visible' => array(
        ':input[name="data[js][edit][navkeys_on]"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['data']['js']['edit']['modal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Modal'),
    '#default_value' => isset($table['data']['js']['edit']['modal']) ? $table['data']['js']['edit']['modal'] : 1,
    '#description' => t('Determines if the dialog will be modal.'),
  );

  $form['data']['js']['edit']['addedrow'] = array(
    '#type' => 'select',
    '#title' => t('Added row position'),
    '#options' => array('first' => t('Top'), 'last' => t('Bottom')),
    '#default_value' => isset($table['data']['js']['edit']['addedrow']) ? $table['data']['js']['edit']['addedrow'] : 'first',
    '#description' => t('	Controls where the row just added is placed'),
  );

  $form['data']['js']['edit']['drag'] = array(
    '#type' => 'checkbox',
    '#title' => t('Draggable'),
    '#default_value' => isset($table['data']['js']['edit']['drag']) ? $table['data']['js']['edit']['drag'] : 1,
    '#description' => t('Determines if the dialog is draggable'),
  );

  $form['data']['js']['edit']['resize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Resizable'),
    '#default_value' => isset($table['data']['js']['edit']['resize']) ? $table['data']['js']['edit']['resize'] : 1,
    '#description' => t('Determines if the dialog can be resized'),
  );

  $form['data']['js']['edit']['dataheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Content height'),
    '#default_value' => isset($table['data']['js']['edit']['dataheight']) ? $table['data']['js']['edit']['dataheight'] : 'auto',
    '#size' => 3,
    '#description' => t('The parameter control the scrolling content - i.e between the modal header and modal footer.'),
  );

  $form['data']['js']['edit']['left'] = array(
    '#type' => 'textfield',
    '#title' => t('Left position'),
    '#default_value' => isset($table['data']['js']['edit']['left']) ? $table['data']['js']['edit']['left'] : 0,
    '#size' => 3,
    '#description' => t('The initial left position of modal dialog.
      The default value of 0 mean the left position from the upper left corner of the grid'),
  );

  $form['data']['js']['edit']['top'] = array(
    '#type' => 'textfield',
    '#title' => t('Top position'),
    '#default_value' => isset($table['data']['js']['edit']['top']) ? $table['data']['js']['edit']['top'] : 0,
    '#size' => 3,
    '#description' => t('The initial top position of modal dialog.
      The default value of 0 mean the top position from the upper left corner of the grid.'),
  );

  $form['data']['js']['edit']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Form width'),
    '#default_value' => isset($table['data']['js']['edit']['width']) ? $table['data']['js']['edit']['width'] : 300,
    '#size' => 3,
    '#description' => t('The width of confirmation dialog'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 20,
    '#validate' => array('_feeds_editor_admin_form_validate'),
    '#submit' => array('_feeds_editor_admin_form_submit'),
  );
  return $form;
}

/**
 * Ajax callback. Returns updated columns depending on chosen feeds id
 */
function feeds_editor_admin_table_columns($form, &$form_state) {
  return $form['data'];
}

/**
 * Validation function. Checks submitted values upon adding/editing worksheets
 */
function _feeds_editor_admin_form_validate($form, &$form_state) {
  
  if (!empty($form_state['values']['db_table'])) {
    $db_table = FEEDS_EDITOR_DB_PREFIX . $form_state['values']['db_table'];
    form_set_value($form['db_table'], $db_table, $form_state);
  }

  $column_settings = $form_state['complete form']['data']['columns']['settings'];
  $edit_rules_settings = array(
    'edithidden',
    'required',
    'number',
    'integer',
  );

  foreach (element_children($column_settings) as $column_name) {
    if (!empty($column_settings[$column_name]['#options'])) {
      foreach ($column_settings[$column_name]['#options'] as $option_name => $option) {

        $new_value = false;
        if (!empty($form_state['values']['data']['columns']['settings'][$column_name][$option_name])) {
          $new_value = true;
        }

        if (in_array($option_name, $edit_rules_settings)) {
          $form_state['values']['data']['columns']['rules'][$column_name][$option_name] = $new_value;
          unset($form_state['values']['data']['columns']['settings'][$column_name][$option_name]);
        }

        $form_state['values']['data']['columns']['settings'][$column_name][$option_name] = $new_value;
      }
    }
  }

  $data = $form_state['values']['data'];
  if (drupal_strlen($data['js']['table']['rowList'])) {
    form_set_value($form['data']['js']['table']['rowList'], explode(',', $data['js']['table']['rowList']), $form_state);
  }
  
  if (strpos($data['js']['edit']['navkeys'], ',') !== FALSE) {
    $navkeys = explode(',', $data['js']['edit']['navkeys']);
    if ($data['js']['edit']['navkeys_on']) {
      array_unshift($navkeys, true);
    }
    else {
      array_unshift($navkeys, false);
    } 
  }
  
  if (!empty($navkeys)) {
    form_set_value($form['data']['js']['edit']['navkeys'], $navkeys, $form_state);
  }
  else {
    unset($form_state['values']['data']['js']['edit']['navkeys']);
  }
  
  if ($data['js']['edit']['savekey_on']) {
    $savekey = array(true, trim($data['js']['edit']['savekey']));
  }
  else {
    $savekey = array(false, trim($data['js']['edit']['savekey']));
  }

  form_set_value($form['data']['js']['edit']['savekey'], $savekey, $form_state);
  
  unset($form_state['values']['data']['js']['edit']['navkeys_on']);
  unset($form_state['values']['data']['js']['edit']['savekey_on']);
  
  $mappings = $form_state['values']['mappings'];

  foreach ($form_state['values']['data']['js'] as $group_name => $group) {
    foreach ($group as $option_name => $option) {
      if ($option === 0) {
        $form_state['values']['data']['js'][$group_name][$option_name] = false;
      }
      elseif ($option === 1) {
        $form_state['values']['data']['js'][$group_name][$option_name] = true;
      }
    }
  }
  
  if (empty($form_state['values']['id'])) {

    if (!$mappings) {
      form_set_error('feeds_id', t('Mapping information is not set.'));
      return;
    }
  
    $database = $form_state['values']['db'];
    
    // Set active database
    db_set_active($database);
    // Check if table already exists. If so, log report error and quit
    if (db_table_exists($db_table)) {
      // Since we've switched to another active database,
      // use native PHP function to log possible errors to prevent "Call to undefined function" error
      trigger_error('Table ' . $db_table . ' already exists in database ' . $database , E_USER_ERROR);
      // Set back default database
      db_set_active();
      // ...and exit
      return;
    }
    
    // Create table in active database
    feeds_editor_create_datatable($db_table, $mappings);
    $form_state['values']['db_created'] = TRUE;
    // Set back default database
    db_set_active();
  }
}

/**
 * Validation function. Saves uploaded files
 */
function feeds_editor_images_form_validate($form, &$form_state) {
  global $user;
  
  //Construct directory path. Each worksheet has own folder for uploaded files
  $directory = FEEDS_EDITOR_UPLOAD_URI . '/table' . $form_state['values']['table']['id'];
  
  // We don't need it anymore
  unset($form_state['values']['table']);
  
  $saved = array();
  if (!empty($form_state['values']['data'])) {
    // Check if target directory exists, create otherwise
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      // Loop over submitted files and create standard managed files according to Drupal File API
      foreach ($form_state['values']['data'] as $upload) {
        
        $file = new stdClass();
        $file->uid = $user->uid;
        $file->status = 1; // Set "permanent" status to avoid deletion on Drupal's scheduled cleanup
        $file->filename = $upload['name'];
        $file->uri = $upload['tmppath'];
        $file->filemime = file_get_mimetype($file->filename);
        $file->filesize = filesize($file->uri);
        $file->source = $upload['tmppath'];
        $file->destination = file_destination($directory . '/' . $file->filename, FILE_EXISTS_RENAME);
        
        // Move from temporary directory
        if (file_unmanaged_move($file->source, $file->destination)) {
          $file->uri = $file->destination;
          // Set permission 0664 on the file
          drupal_chmod($file->uri);
          // Save the file
          if ($saved_file = file_save($file)) {
            // Let Drupal know that the file owned by Feeds Editor module
            file_usage_add($saved_file, 'feeds_editor', 'file', $file->fid);
            $saved[] = $saved_file;
          }
        }
      }
    }
    // If nothing saved, log it and exit
    if (!$saved) {
      form_set_error('', t('An error occurred. No file saved.'));
      return;
    }
    // Seems all right, set saved files and pass to submit function
    form_set_value($form['data'], $saved, $form_state);
  }
}

/**
 * Submit function. Create new/update existing worksheet
 */
function _feeds_editor_admin_form_submit($form, &$form_state) {
  
  if (!empty($form_state['values']['db_created'])) {
    drupal_set_message(t('Database table %table in database %db created', array(
      '%table' => $form_state['values']['db_table'],
      '%db' => $form_state['values']['db'],
    )));
  }
  
  // Get actual columns which described in hook_schema
  $schema = drupal_get_schema('feeds_editor_tables');
  $allowed_fields = array_keys($schema['fields']);
  
  // Filter recieved values
  foreach ($form_state['values'] as $key => &$value) {
    if (!in_array($key, $allowed_fields)) {
      unset($form_state['values'][$key]);
    }
  }
  
  // Add/edit the table
  $action = feeds_editor_table_set($form_state['values']);

  if ($action == 1) {
    drupal_set_message(t('Table %name successfully created', array(
      '%name' => $form_state['values']['name'])));
  }
  elseif ($action == 2 && isset($form_state['values']['name'])) {
    drupal_set_message(t('Table %name successfully updated', array(
      '%name' => $form_state['values']['name'])));
  }
  $form_state['redirect'] = 'admin/structure/feeds-editor';
}

/**
 * Confirmation form. Requires user to confirm deletion of specific worksheet
 */
function feeds_editor_table_delete_confirm($form, &$form_state, $table) {
  $form['table'] = array('#type' => 'value', '#value' => $table);
  return confirm_form($form, t('Are you sure you want to delete @name?', array(
    '@name' => $table['name'])), 'admin/structure/feeds-editor');
}

/**
 * Confirmation form. Requires user to confirm deletion of specific upload
 */
function feeds_editor_upload_delete_confirm($form, &$form_state, $upload) {
    
    $form['upload'] = array('#type' => 'value', '#value' => $upload);
  return confirm_form($form, t('Are you sure you want to delete @name?', array(
    '@name' => $upload->description)), 'admin/structure/feeds-editor/files', t('All files from this upload will be deleted as well.') .
      ' ' . t('This action cannot be undone.'));

}

/**
 * Submit function. Deletes specific upload after the action was confirmed
 */
function feeds_editor_upload_delete_confirm_submit($form, &$form_state) {

  if ($files = unserialize($form_state['values']['upload']->data)) {
    foreach ($files as $value) {
      $file = file_load($value->fid);
      file_delete($file);
    }
  }
  
  feeds_editor_upload_delete($form_state['values']['upload']->id);
  drupal_set_message(t('Upload %name successfully deleted', array(
    '%name' => $form_state['values']['upload']->description)));
  
  $form_state['redirect'] = 'admin/structure/feeds-editor/files';
}

/**
 * Submit function. Deletes specific worksheet after the action was confirmed
 */
function feeds_editor_table_delete_confirm_submit($form, &$form_state) {

  $db_table = $form_state['values']['table']['db_table'];
  $db = $form_state['values']['table']['db'];
  
  $dropped = FALSE;
  
  // Switch to working database and delete corresponding worksheet table in it
  if (!db_set_active($db)){
    drupal_set_message(t('Could not connect to database %db', array('%db' => $db)), 'error');
    return;
  }
  
  // Remove the table
  db_drop_table($db_table);
  $dropped = TRUE;
  
  // Set back default database
  db_set_active();

  if ($dropped) {
    drupal_set_message(t('Database table %table in database %db was removed', array(
      '%table' => $db_table,
      '%db' => $db,
    )));
  }
  
  // After the working worksheet table was dropped, delete all remaining information
  // that stored in "feeds_editor_tables" table
  if (feeds_editor_table_delete($form_state['values']['table']['id'])) {
    drupal_set_message(t('Table %name successfully deleted', array(
      '%name' => $form_state['values']['table']['name'])));
    
    $form_state['redirect'] = 'admin/structure/feeds-editor';
  }
}

/**
 * Form builder. Used to bulk upload
 */
function feeds_editor_images_form($form, &$form_state, $table) {
  
  // Get max existing value in "feeds_editor_files" table
  $max = db_query("SELECT MAX(id) FROM {feeds_editor_files}")->fetchField();

  $form['table'] = array('#type' => 'value', '#value' => $table);
  $form['timestamp'] = array('#type' => 'value', '#value' => time());
  
  // Plupload widget
  $form['data'] = array(
    '#title' => t('Upload files'),
    '#type' => 'plupload',
  );
  
  // Description (name) for administrative purpose
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => t('Upload #@num', array('@num' => $max + 1)),
    '#description' => t('An optional description for administrator.')
  );
  
  // An optional tag for this upload
  $form['tag'] = array(
    '#title' => t('Tag'),
    '#type' => 'textfield',
    '#maxlength' => 255,
    '#autocomplete_path' => 'feeds-editor/tags',
    '#default_value' => check_plain($table['name']),
    '#description' => t('Tag this upload to easy <a href="@url" target="_blank">find</a> it in the future.
      This is autocomplete field, so you can select previously entered tags.', array(
        '@url' => url('admin/structure/feeds-editor/files'))),
  );
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#tree' => FALSE,
  );
  return $form;
}

function feeds_editor_table_form($form, &$form_state, $table) {
  // Load feeds importer
  $importer = feeds_importer_load($table['feeds_id']);

  $form['importer'] = array('#type' => 'value', '#value' => $importer);
  $form['table'] = array('#type' => 'value', '#value' => $table);
  
  // Load JqGrid
  $form['grid'] = array(
    '#markup' => _feeds_editor_table($table, $importer),
  );
  
  $form['actions'] = array('#type' => 'actions', '#weight' => 100);
  $form['actions']['convert_csv'] = array(
    '#name' => 'convert_csv',
    '#type' => 'submit',
    '#value' => t('Convert to CSV'),
  );
  
  if ($importer->config['fetcher']['plugin_key'] == 'FeedsFileFetcher' &&
    $importer->config['parser']['plugin_key'] == 'FeedsCSVParser') {

    $form['actions']['convert_csv_run'] = array(
      '#name' => 'convert_csv_run',
      '#type' => 'submit',
      '#value' => t('Convert to CSV and import'),
    );
  }
  
  // Created CSV files are stores in private directory.
  // Check if private directory is set, warn user otherwise
  if (!variable_get('file_private_path', NULL)) {

    $form['actions']['convert_csv']['#disabled'] = TRUE; // Disable the button
    $message = t('Exported CSV files will be stored in private file directory for security reason.
      You must <a href="@url">configure</a> your existing local file system path for storing private files.', array(
        '@url' => url('admin/config/media/file-system')));

    drupal_set_message($message, 'warning');
  }
  return $form;
}

/**
 * Submit function. Creates/updates file upload
 */
function feeds_editor_images_form_submit($form, &$form_state) {
  
  // Clean up submitted values
  form_state_values_clean($form_state);
  
  // Check if "data" is not empty to prevent double inserting
  if (!empty($form_state['values']['data'])) {  
    feeds_editor_upload_set($form_state['values']);
  }
}

/**
 * Submit function. Different actions for specific worksheet
 */
function feeds_editor_table_form_submit($form, &$form_state) {
  
  $operation = $form_state['clicked_button']['#name'];
  $importer = $form_state['values']['importer'];
  $table = $form_state['values']['table'];
  $db_table = $table['db_table'];
  
  // Try to set active database, warn user if unable to perform the action
  if (!db_set_active($table['db'])){
    drupal_set_message(t('Could not connect to database %db', array('%db' => $table['db'])), 'error');
    return;
  }
  
  // Check if working table exists in active database, display error if not.
  if (!db_table_exists($db_table)) {
    trigger_error('Table ' . $db_table . ' does not exists in database ' . $database , E_USER_ERROR);
    db_set_active();
    return;
  }
  
  // Get all rows of the table
  $rows = feeds_editor_row_get($db_table);
  
  // Set back default active database
  db_set_active();
  
  if (!empty($rows)) {
    // Get importer's mapping information
    $mapping = feeds_editor_importer_mapping($importer);
    $options = array(
      'table_id' => $table['id'],
      'table_name' => $table['name'],
      'columns' => $mapping['source'],
      'rows' => $rows,
    );
  }
  
  // Define batch structure
  $batch = array(
    'operations' => array(array('feeds_editor_export_csv_batch', array($options))),
    'finished' => 'feeds_editor_export_csv_batch_finished',
    'file' => drupal_get_path('module', 'feeds_editor') . '/feeds_editor.generate.csv.inc',
  );
  
  if ($operation == 'convert_csv') {
    // Run batch process
    batch_set($batch);
    
    // Redirect after the batch finished
    batch_process('admin/structure/feeds-editor/' . $table['id'] . '/table');
  }
  elseif ($operation == 'convert_csv_run') {
    
    // Run batch process
    batch_set($batch);
    
    // Check $_SESSION for stored uri of created CSV file
    if (!empty($_SESSION['feeds_editor']['export_csv'])) {
      $destination = $_SESSION['feeds_editor']['export_csv'];
      // Perform import
      feeds_editor_process_feed($table['feeds_id'], $destination, $table['id']);
    }
  }
}
