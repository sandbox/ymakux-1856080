<?php

/**
* Batch operation to CSV export.
*/
function feeds_editor_export_csv_batch($options, &$context) {

  $context['finished'] = 0;
  if (!isset($context['sandbox']['file'])) {

    // Create the file and print the labels in the header row.
    $filename = 'table_' . $options['table_id'] . '.csv';
    
    $file_path = file_directory_temp() . '/' . $filename;
    // Create the file.
    $handle = fopen($file_path, 'w'); 
    // Write the labels to the header row.
    fputcsv($handle, $options['columns']); 
    fclose($handle);

    $context['sandbox']['file'] = $file_path;
    $context['sandbox']['rows'] = $options['rows'];
    $context['sandbox']['rows_total'] = count($context['sandbox']['rows']);

    // Store some values in the results array for processing when finished.
    $context['results']['filename'] = $filename;
    $context['results']['file'] = $file_path;
    $context['results']['table_id'] = $options['table_id'];
    $context['results']['table_name'] = $options['table_name'];
  }

  // Accounting.
  if (!isset($context['results']['count'])) {
    $context['results']['count'] = 0;
  }

  // Open the file for writing ('a' puts pointer at end of file).
  $handle = fopen($context['sandbox']['file'], 'a');

  // Loop until we hit the batch limit.
  for ($i = 0; $i < variable_get('feeds_editor_export_batch_limit', 50); $i++) {
    $number_remaining = count($context['sandbox']['rows']);

    if ($number_remaining) {

      $row_data = (array) $context['sandbox']['rows'][$context['results']['count']];
      unset($row_data['id']);
      
      // Build CSV string
      fputcsv($handle, $row_data);

      // Remove the row from $context.
      unset($context['sandbox']['rows'][$context['results']['count']]);

      // Increment the counter.
      $context['results']['count']++;
      $context['finished'] = $context['results']['count'] / $context['sandbox']['rows_total'];
    }
    // If there are no rows remaining, we're finished.
    else {
      $context['finished'] = 1;
      break;
    }
  }

  // Close the file.
  fclose($handle);

  // Show message how many rows have been exported.
  $context['message'] = t('Exported @count of @total rows.', array(
    '@count' => $context['results']['count'],
    '@total' => $context['sandbox']['rows_total'],
  ));
}

/**
* Finish the export.
*/
function feeds_editor_export_csv_batch_finished($success, $results, $operations) {
  
  $download = '';

  // The 'success' parameter means no fatal PHP errors were detected.
  if ($success) {
    $message = format_plural($results['count'], 'One row exported.', '@count rows exported.');
    global $user;
    $directory = 'private://feeds-editor/table' . $results['table_id'];
    // Check if destination directory exists, create otherwise
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      
      // Create standard managed file according to Drupal File API
      $file = new stdClass();
      $file->uid = $user->uid;
      $file->status = 1; // Set permanent flag to prevent deleting during Drupal's scheduled file cleanup
      $file->filename = $results['filename'];
      $file->uri = $results['file'];
      $file->filemime = file_get_mimetype($file->filename);
      $file->filesize = filesize($file->uri);
      $file->source = $file->uri;
      $file->destination = file_destination($directory . '/' . $file->filename, FILE_EXISTS_RENAME);
      $file->filename = basename($file->destination);
      
      // Move from temporary directory
      if (file_unmanaged_move($file->source, $file->destination)) {
        $file->uri = $file->destination;
        // Set permission 0664 on the file
        drupal_chmod($file->uri);
        if ($file = file_save($file)) {
          // Let Drupal know that the file owned by Feeds Editor module
          file_usage_add($file, 'feeds_editor', 'file', $file->fid);
          // Get max existing id value
          $max = db_query("SELECT MAX(id) FROM {feeds_editor_files}")->fetchField();
          
          // Buld array of values
          $params = array(
            'timestamp' => time(),
            'data' => array($file),
            'description' => t('Upload #@num (@mime)', array('@num' => $max + 1, '@mime' => $file->filemime)),
            'tag' => $results['table_name'],
          );
          
          // Write record in DB about this upload
          feeds_editor_upload_set($params);
          
          // Message for user
          $url = 'system/files/feeds-editor/table' . $results['table_id'] . '/' . $file->filename;
          $download = '<p>' . t('Your file is now ready for download, !download_link.', array(
            '!download_link' => l(t('click here to download the file'), $url))) . '</p>';
          
          // Memorize saved CSV file
          $_SESSION['feeds_editor']['export_csv'] = $file;
        }
      }
    }
  }
  else {
    // Something gone wrong, tell about it.
    $message = t('There were errors during the export.');
  }
  // Set message
  drupal_set_message($message . $download);
}
