<?php
/**
 * @file
 * Installation and uninstallation functions.
 */

/**
 * Implements hook_schema().
 */
function feeds_editor_schema() {
  $schema['feeds_editor_tables'] = array(
    'description' => 'Base table for Feeds Editor',
    'fields' => array(
      'id' => array(
        'description' => 'An autoincrement ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Table\'s human name',
        'type' => 'varchar',
        'length' => 255,
      ),
      'description' => array(
        'description' => 'Table description',
        'type' => 'varchar',
        'length' => 255,
      ),
      'feeds_id' => array(
        'description' => 'Feeds ID',
        'type' => 'varchar',
        'length' => 255,
      ),
      'db_table' => array(
        'description' => 'Database machine name',
        'type' => 'varchar',
        'length' => 64,
      ),
      'db' => array(
        'description' => 'Working database',
        'type' => 'varchar',
        'length' => 64,
      ),
      'data' => array(
        'description' => 'Serialized array with the various settings and info',
        'type' => 'text',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );
  $schema['feeds_editor_files'] = array(
    'description' => 'Contains Feeds Editor\'s uploaded files',
    'fields' => array(
      'id' => array(
        'description' => 'An autoincrement ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'description' => array(
        'description' => 'Upload name',
        'type' => 'varchar',
        'length' => 255,
      ),
      'tag' => array(
        'description' => 'Upload tag',
        'type' => 'varchar',
        'length' => 255,
      ),
	    'timestamp' => array(
        'description' => 'UNIX timestamp',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
		    'default' => 0,
      ),
      'data' => array(
        'description' => 'Serialized array with the various settings and info',
        'type' => 'text',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}
