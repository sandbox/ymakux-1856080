<?php

/**
 * Theme function. List of all available worksheets
 */
function theme_feeds_editor_overview($variables) {

  $header = array(
    array('data' => t('Name'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Description')),
    array('data' => t('Feeds ID'), 'field' => 'feeds_id'),
    array('data' => t('Database'), 'field' => 'db'),
    array('data' => t('Table'), 'field' => 'db_table'),
    array('data' => t('Actions')),
  );
  
  $query = db_select('feeds_editor_tables', 't')
    ->fields('t')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(10);
    
  $result = $query->orderByHeader($header)->execute();

  $rows = array();
  if ($result) {
    foreach ($result as $table) {
      $links = array();
      $links[] = array(
        'title' => t('Edit'),
        'href' => 'admin/structure/feeds-editor/' . $table->id . '/edit',
      );
      $links[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/feeds-editor/' . $table->id . '/delete',
      );
      $links[] = array(
        'title' => t('Worksheet'),
        'href' => 'admin/structure/feeds-editor/' . $table->id . '/table',
      );
      $actions = theme('ctools_dropdown', array('title' => t('Select'), 'links' => $links));
      $rows[] = array(
        array('data' => check_plain($table->name)),
        array('data' => check_plain($table->description)),
        array('data' => check_plain($table->feeds_id)),
        array('data' => check_plain($table->db)),
        array('data' => check_plain($table->db_table)),
        array('data' => $actions),
      );
    }
  }
  $empty = t('You have no tables yet.');
  return theme('table', array('rows' => $rows, 'header' => $header, 'empty' => $empty)) . theme('pager');
}

/**
 * Theme function. Returns JqGrid table
 */
function theme_feeds_editor_jqgrid($variables) {

  $output = '';
  $table = $variables['table'];
  $js_settings = $variables['settings'];

  $table_id = 'data-table';
  drupal_add_library('feeds_editor', 'jqgrid');
  drupal_add_library('system', 'ui.resizable');

  $js = '
    (function ($) {
      Drupal.behaviors.clickToCopy = {
        attach: function (context, settings) {
          $("#uploaded-urls", context).click(function() {
            $(this).select().addClass("selected");
          });
        }
      };
    })(jQuery);';
  
  drupal_add_js($js, 'inline');
  feeds_editor_js_config($table_id, $js_settings, $table);

  if (!empty($table['data']['theme'])) {
    drupal_add_css($table['data']['theme']);
  }

  $links = array(); 
  $links[] = array(
    'title' => t('Upload files'),
    'href' => 'admin/structure/feeds-editor/nojs/' . $table['id'] . '/images',
    'attributes' => array('class' => array('ctools-use-modal')),
  );

  $links[] = array(
    'title' => t('Customize'),
    'href' => 'admin/structure/feeds-editor/nojs/' . $table['id'] . '/customize',
    'attributes' => array('class' => array('ctools-use-modal')),
  );

  $links[] = array(
    'title' => t('Legend'),
    'href' => 'admin/structure/feeds-editor/' . $table['id'] . '/legend',
    'attributes' => array(
      'onclick' => 'window.open(this.href,"name","width=800,scrollbars=yes");return false',
    ),
  );

  $output .= theme('ctools_dropdown', array('title' => t('Select'), 'links' => $links));
  $output .= '<table id="' . $table_id . '"></table>';
  $output .= '<div id="pagered" ></div>';
  return $output;
}

/**
 * Theme function. Outputs uploaded files in modal window
 */
function theme_feeds_editor_uploaded($variables) {

  $items = array();
  $urls = array();
  foreach ($variables['files'] as $file) {
    $url = file_create_url($file->uri);
    $items[] = l(check_plain($file->filename), $url);
    $urls[] = $url;
  }

  $output = '<h3>' . t('Successfully saved:') . '</h3>';
  $output .= '<div id="uploaded-items">' . theme('item_list', array(
    'items' => $items)) . '</div>';

  $output .= '<h3>' . t('URLs') . '</h3>';
  $output .= '<textarea id="uploaded-urls"
    style="padding: 10px; border: 1px solid #ccc;background: #f2f2f2;width: 90%;">';

  $output .= implode(variable_get('feeds_editor_urls_delimiter', ','), $urls);
  $output .= '</textarea>';
  $output .= '<div class="description">';
  $output .= t('Click and copy saved URLs, then paste them directly into appropriate table cell.');
  $output .= '</div>';
  return $output;
}

/**
 * Theme function. Help table that explains available targets and sources
 */
function theme_feeds_editor_mapping_legend($variables) {

  module_load_include('inc', 'feeds_ui', 'feeds_ui.admin');
  $form = drupal_get_form('feeds_ui_mapping_form', $variables['importer']);

  // Build the actual mapping table.
  $header = array(
    t('Source'),
    t('Target'),
    t('Unique target'),
    '&nbsp;',
  );
  $rows = array();
  if (is_array($form['#mappings'])) {
    foreach ($form['#mappings'] as $i => $mapping) {
      // Some parsers do not define source options.
      $source = isset($form['source']['#options'][$mapping['source']]) ? $form['source']['#options'][$mapping['source']] : $mapping['source'];
      $target = isset($form['target']['#options'][$mapping['target']]) ? check_plain($form['target']['#options'][$mapping['target']]) : '<em>' . t('Missing') . '</em>';
      $rows[] = array(
        check_plain($source),
        $target,
        drupal_render($form['unique_flags'][$i]),
        drupal_render($form['remove_flags'][$i]),
      );
    }
  }
  if (!count($rows)) {
    $rows[] = array(
      array(
        'colspan' => 4, 
        'data' => t('No mappings defined.'),
      ),
    );
  }
  $rows[] = array(
    drupal_render($form['source']),
    drupal_render($form['target']),
    '',
    drupal_render($form['add']),
  );
  $output = '<div class="help feeds-admin-ui""' . drupal_render($form['help']) . '</div>';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));

  // Build the help table that explains available sources.
  $legend = '';
  $rows = array();
  foreach (element_children($form['legendset']['legend']['sources']) as $k) {
    $rows[] = array(
      check_plain(drupal_render($form['legendset']['legend']['sources'][$k]['name'])),
      check_plain(drupal_render($form['legendset']['legend']['sources'][$k]['description'])),
    );
  }
  if (count($rows)) {
    $legend .= '<h4>' . t('Sources') . '</h4>';
    $legend .= theme('table', array('header' => array(t('Name'), t('Description')), 'rows' => $rows));
  }

  // Build the help table that explains available targets.
  $rows = array();
  foreach (element_children($form['legendset']['legend']['targets']) as $k) {
    $rows[] = array(
      check_plain(drupal_render($form['legendset']['legend']['targets'][$k]['name'])),
      check_plain(drupal_render($form['legendset']['legend']['targets'][$k]['description'])),
    );
  }
  $legend .= '<h4>' . t('Targets') . '</h4>';
  $legend .= theme('table', array('header' => array(t('Name'), t('Description')), 'rows' => $rows));
  return $legend;
}

/**
 * Theme function. List of available uploads
 */
function theme_feeds_editor_files($variables) {

  $header = array(
    array('data' => t('Description'), 'field' => 'description'),
    array('data' => t('Tag'), 'field' => 'tag'),
    array('data' => t('Uploaded'),'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Actions')),
  );

  $query = db_select('feeds_editor_files', 'f')
    ->fields('f')
    ->extend('TableSort')
    ->extend('PagerDefault')
    ->limit(10);
    
  $result = $query->orderByHeader($header)->execute();
  $rows = array();
  if ($result) {
    foreach ($result as $upload) {
      $links = array();
      $links[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/feeds-editor/uploads/' . $upload->id . '/delete',
      );
      $links[] = array(
        'title' => t('List'),
        'href' => 'admin/structure/feeds-editor/uploads/' . $upload->id . '/list',
      );
      $actions = theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline'))));
      $rows[] = array(
        array('data' => check_plain($upload->description)),
        array('data' => check_plain($upload->tag)),
        array('data' => format_date($upload->timestamp)),
        array('data' => $actions),
      );
    }
  }
  $empty = t('You have no uploaded files yet.');
  return theme('table', array('rows' => $rows, 'header' => $header, 'empty' => $empty)) . theme('pager');
}

/**
 * Theme function. List of files for specific upload
 */
function theme_feeds_editor_files_upload($variables) {

    $upload = $variables['upload'];
    drupal_set_title(t('Files of @upload', array('@upload' => $upload->description)));

    $header = array(
      array('data' => t('File')),
      array('data' => t('Path')),
    );
    
    $rows = array();
    $files = unserialize($upload->data);
    
    foreach ($files as $file) {
      $rows[] = array(
        array('data' => check_plain($file->filename)),
        array('data' => l(drupal_realpath($file->uri), file_create_url($file->uri))),
      );
    }
  
  $empty = t('This upload contains no files.');
  return theme('table', array('rows' => $rows, 'header' => $header, 'empty' => $empty));
}

/**
 * Theme function. Column settings admin form
 */
function theme_feeds_editor_admin_table_columns($variables) {
  $rows = array();
  $header = array();
  
  if (!empty($variables['element']['settings'])) {
    foreach(element_children($variables['element']['settings']) as $column) {
      $header[] = substr($column, 0, strpos($column, '|'));
      $row = drupal_render($variables['element']['settings'][$column]);
      $row .= drupal_render($variables['element']['rules'][$column]);
      $rows[0][$column] = $row;
    }

  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}
