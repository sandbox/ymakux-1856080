The Feeds Editor module intended to make your life easier when you work with CSV files for Feeds importers.
 
Installation
============
1. Download the JqGrid library from http://www.trirand.com/blog/?page_id=6. Select all modules to be sure that we get all needed stuff.
2. Create folder "jqgrid" in "sites/all/libraries" directory.
   Expected files structure:
   - sites/all/libraries/jqgrid
   -- js/jquery.jqGrid.min.js
   -- css/ui.jqgrid.css
3. Download and enable all required modules: Libraries API(http://drupal.org/project/libraries),
  Plupload integration (http://drupal.org/project/plupload) and, of course Feeds (http://drupal.org/project/feeds) modules
4. Enable this module

Usage
=====
1. Create a feeds importer
2. Go to admin/structure/feeds-editor/add and add a new worksheet for desired importer
3. Go to worksheet (on overview table, click "Worksheet" link) admin/structure/feeds-editor/<ID>/table and some rows in it
4. After you fineshed, click "Convert to CSV" - to generate CSV file OR "Convert to CSV and import" - to generate CSV file and start import.

All your uploaded files are stored at admin/structure/feeds-editor/files

Author
======
Iurii Makukh
ymakux@gmail.com